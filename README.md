# Debian/Ubuntu repo hosting using Gitlab Pages

This is an example project how to create a gitlab.com hosted apt repo using Gitlab Pages.

The important steps are `reprepro` which renders the repository data and `pages`, which finally uploads the relevant data to the pages http server. `package` is just an example step, replace with your real package creation step (mind the `artifacts`!).

For signing your repo metadata (recommended!), you'll need to add an environment variable `SIGNING_KEY_ID` containing your private key ([Howto create a subkey](https://www.digitalocean.com/community/tutorials/how-to-use-reprepro-for-a-secure-package-repository-on-ubuntu-14-04)). 

The final repo will be available under your pages domain.

## Examples:

* [SOGo](https://gitlab.com/packaging/sogo/)
* [piler](https://gitlab.com/packaging/piler/)

The same logic should work for rpm based repos using `createrepo` too :grin: